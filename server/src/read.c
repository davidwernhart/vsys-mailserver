#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include "filehandler.h"
#include "myserver.h"
#include "read.h"

char pathbuf[64];
char returnstr[1024];

int setuser(char * name){
  char username[16];
  strcpy(username,name);
  char* path = getuserpath(username);
  DIR* dir = opendir(path);
  if (dir){
    closedir(dir);
    strcpy(pathbuf,path);
    sendsocket("OK\n");
    return 0;
  }
  else{
    sendsocket("ERR\n");
    return 1;
  }
}

void setmsgid(char * msgid){
  msgid[strlen(msgid)-1] = 0;
  strcat(pathbuf,msgid);
  if( access( pathbuf, F_OK ) != -1 ){
    snprintf(returnstr,1024,"OK\nSENDER: ");
    FILE * f = openfile(pathbuf,-1,"r");
    char buffer[100];
    while(fgets(buffer, 100, f)) {
      strcat(returnstr,buffer);
    }
    closefile(f);
    sendsocket(returnstr);
  } else {
    sendsocket("ERR\n");
  }
}
