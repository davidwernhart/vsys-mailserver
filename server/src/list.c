#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "list.h"
#include "filehandler.h"
#include "myserver.h"

FILE* f;
char returnstr[1024];

void list(char * name){
  char username[16];
  strcpy(username,name);
  char* path = getuserpath(username);
  int filecount = getfilecount(path);
  if(filecount > 0)
    filecount = filecount - 1;
  snprintf(returnstr,1024,"NUMBER OF MESSAGES: %d\n",filecount);
  for(int i = 1; i <= filecount; i++){
    f = openfile(path,i,"r");
    if(f != NULL){
      char buffer[81];
      fgets(buffer, 81, f);
      fgets(buffer, 81, f);
      char idbuf[16];
      sprintf(idbuf,"%d: ",i);
      strcat(returnstr,idbuf);
      strcat(returnstr,buffer);
      closefile(f);
    }
  }
  sendsocket(returnstr);
}
