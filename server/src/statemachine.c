#include <stdlib.h>
#include <stdio.h>
#include "statemachine.h"
#include "login.h"
#include "send.h"
#include "list.h"
#include "read.h"
#include "delete.h"
#include "myserver.h"

int state = 0;
int cmd = -1;

void parsemsg(char * msg){

  printf("MESSAGE LENGTH: %d\n", strlen(msg) );

  if(cmd == -1){ //login
    if(state == 0){
      if(startlogin(msg) == 0){
        state ++;
      }
    }
    else if(state == 1){
      if(enterusr(msg)){
        state = 0;
      }
      else{
        state ++;
      }
    }
    else if(state == 2){
      if(enterpwd(msg)){
        state = 0;
      }
      else{
        state = 0;
        cmd = 0;
      }
    }
  }
  else if(cmd == 0){ //hub for choosing commands
    getcmd(msg);
  }
  else if(cmd == 1){ //send

    if(state == 0){
      if(setreceiver(msg))
        cmd = 0;
      else
        state ++;
    }
    else if(state == 1){
      if(setsubject(msg))
        cmd = 0;
      else
        state ++;
    }
    else if(state == 2){
      if(setmessage(msg))
        cmd = 0;
    }
  }

//cmd 2 is list but does not need a handler anymore

  else if(cmd == 3){ //read
    setmsgid(msg);
    cmd = 0;
  }
  else if(cmd == 4){ //del
    setdelmsg(msg);
    cmd = 0;
  }

  flush();
  printf("STATE:%d COMMAND:%d\n",state,cmd);
}

void getcmd(char * msg){
  state = 0;
  if(strcmp(msg,"SEND\n") == 0){
    if(setsender(usr) == 0)
      cmd = 1;
  }
  else if(strcmp(msg,"LIST\n") == 0){
    list(usr);
  }
  else if(strcmp(msg,"READ\n") == 0){
    if(setuser(usr) == 0)
      cmd = 3;
  }
  else if(strcmp(msg,"DEL\n") == 0){
    if(setdeluser(usr) == 0)
        cmd = 4;
  }
  else if(strcmp(usr,"QUIT\n") == 0){
    //return EXIT_FAILURE;
    sendsocket("OK\n");
  }
  else{
    printf("wrong command entered!");
    cmd = 0;
    sendsocket("ERR\n");
  }
}

void resetstates(){
  state = 0;
  cmd = 0;
}
