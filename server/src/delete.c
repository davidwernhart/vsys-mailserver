#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include "filehandler.h"
#include "myserver.h"
#include "delete.h"

char pathbuf[64];
char folderbuf[64];

int setdeluser(char * name){
  char username[16];
  strcpy(username,name);
  char* path = getuserpath(username);
  DIR* dir = opendir(path);
  if (dir){
    if(strstr(username,"..") != NULL){
      sendsocket("NICE TRY!\n");
      return 1;
    }
    else{
      closedir(dir);
      strcpy(folderbuf,path);
      sendsocket("OK\n");
      return 0;
    }
  }
  else{
    sendsocket("ERR\n");
    return 1;
  }
}

void setdelmsg(char * msgid){
  msgid[strlen(msgid)-1] = 0;
  int index = atoi(msgid);
  if(index != 0){
    strcpy(pathbuf,folderbuf);
    strcat(pathbuf,msgid);
    if( access( pathbuf, F_OK ) != -1 ){
      printf("index: %d\n",index);
      if(remove(pathbuf) == 0){
        if(getfilecount(folderbuf) == 0){
          remove(folderbuf);
        }
      // else if(index < getlastid(folderbuf,0)){
      //   recrename(folderbuf,index);
      // }
      sendsocket("OK\n");
      }
      else{
        sendsocket("ERR\n");
      }

    } else {
      sendsocket("ERR\n");
    }
  }
  else{
    sendsocket("ERR\n");
  }
}

//deprecated
void recrename(char * path,int id){
  printf("PATH: %s\n",path);
  char oldpath[64] = "";
  char newpath[64] = "";
  char idbuf[16] = "";
  strcpy(oldpath,path);
  strcpy(newpath,path);
  snprintf(idbuf,16,"%d",id);
  strcat(newpath,idbuf);
  snprintf(idbuf,16,"%d",id+1);
  strcat(oldpath,idbuf);

  printf("OLDPATH: %s\n",oldpath);
  printf("NEWPATH: %s\n",newpath);
  rename(oldpath,newpath);

  if((id+1) < getlastid(path,0))
    recrename(path,id + 1);
}
