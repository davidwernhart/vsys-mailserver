#include <stdlib.h>
#include <stdio.h>

#ifndef _FILEHANDLER_
#define _FILEHANDLER_

extern char basepath[16];

FILE* openfile(char* path, int id, char* mode);
void writemsg(FILE* f, char* msg);
void closefile(FILE* f);
void createpath(char* path);
char* getuserpath(char* username);
int getlastid(char* path, int create);
int getfilecount(char*path);
void updateindex(FILE* f, int index);
#endif
