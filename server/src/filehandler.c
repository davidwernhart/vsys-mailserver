#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "filehandler.h"

char basepath[16];
//if id is lower than zero, it gets ignored
FILE* openfile(char* path, int id, char* mode){

  char mypath[64] = "";
  char idbuf[16] = "";
  strcpy(mypath,path);
  if(id >= 0){
    snprintf(idbuf,16,"%d",id);
    strcat(mypath,idbuf);
  }
  FILE * f = fopen(mypath,mode);
  
  if(f == NULL){
     perror(mypath);
     return NULL;
     //return EXIT_FAILURE;
  }
  else
    return f;
}

void writemsg(FILE *f, char* msg){
  fprintf(f,msg);
}

void closefile(FILE *f){
  fclose(f);
}

char* getuserpath(char* username){
  username[strlen(username)-1] = 0;
  char *buf = malloc(64);
  strcpy(buf,basepath);
  strcat(buf,username);
  strcat(buf,"/");
  return buf;
}

void createpath(char* path ){
  struct stat st = {0};
  if (stat(path, &st) == -1){
    mkdir(path,0700);

    FILE* f = openfile(path,0,"w");
    writemsg(f,"0");
    closefile(f);
  }
}

int getlastid(char* path, int create){

  if(create) 
    createpath(path);
  
  FILE* f = openfile(path,0,"r+");
  if (f == NULL){
    closefile(f);
    printf("FILE IS NULL");
    return -1;
  }

  int fd = fileno(f);  //get file descriptor necessary for locking
  struct flock lock;
  memset (&lock, 0, sizeof(lock));
  lock.l_type = F_RDLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = 0;
  lock.l_len = 0; 
  if (fcntl(fd, F_SETLKW, &lock) == -1) { // lock file
    closefile(f);
    printf("FILE LOCK DID NOT WORK");
    return -1;
  }

  fpos_t pos; //gets start pointer of file
  fgetpos(f,&pos);

  char buffer[16];
  fgets(buffer, 16, f);
  printf("INDEX BUFFER: %s\n",buffer);
  int num = atoi(buffer);
  printf("LAST INDEX IS: %d\n",num);

  if(create){
    fsetpos(f,&pos);
    fputs("                 ",f);
    fsetpos(f,&pos);
    fprintf(f,"%d",num+1);
    fflush(f);
  }

  lock.l_type = F_UNLCK;
  if (fcntl(fd, F_SETLK, &lock) == -1) { //unlock file
    closefile(f);
    printf("FILE UNLOCK DID NOT WORK");
    return -1;
  }

  closefile(f);
  return num;
}

//deprecated
int getfilecount(char*path){
  int file_count = 0;
  DIR * dirp;
  struct dirent * entry;

  dirp = opendir(path); /* There should be error handling after this */

  if(!dirp)
    return 0;

  while ((entry = readdir(dirp)) != NULL) {
    if (entry->d_type == DT_REG) { /* If the entry is a regular file */
         file_count++;
    }
  }
  closedir(dirp);
  printf("PATH CONTAINS %d ITEMS!\n",file_count);
  return file_count;
}

//deprecated
void updateindex(FILE* f, int index){
  fprintf(f,"%d",index);
  closefile(f);
}