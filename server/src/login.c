#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "login.h"
#include "filehandler.h"
#include "myserver.h"

#define blacklistcooldown 30//cooldown time in seconds
char usr[9];
char pwd[32];

int attempt = 0;

int startlogin(char* keyword){
    if(strcmp(keyword,"LOGIN\n") == 0){
        char *buf = malloc(64);
        strcpy(buf,basepath);
        strcat(buf,"/blacklist");
        FILE* f = openfile(buf,-1,"a+");

        fpos_t pos; //gets start pointer of file
        fgetpos(f,&pos);


        if(attempt > 3){
            attempt = 0;
            fprintf(f,"%s,%lu#\n",clientip,(unsigned long)time(NULL));
            printf("TOO MANY LOGIN ATTEMPTS FROM %s!\n",clientip);
        }
        else
            attempt ++;

        fsetpos(f,&pos);
        char* readbuf = malloc(64);
        while( !feof( f ) )
        {
            fscanf(f,"%s,%s\n",readbuf);

            char* ipbuf = malloc(32);
            char* timebuf = malloc(32);
            int bracket = 0;

            for(int i = 0; i < strlen(readbuf);i++){
                if(readbuf[i] == ','){
                    ipbuf[i] = '\0';
                    bracket = i;
                    break;
                }
                else
                    ipbuf[i]=readbuf[i];
            }
            for(int i = 0; i < strlen(readbuf);i++){
                if(readbuf[i + bracket + 1] == '#'){
                    timebuf[i] = '\0';
                    break;
                }
                else
                    timebuf[i]=readbuf[i + bracket + 1];
            }

            unsigned long timestamp = strtoul(timebuf, NULL, 10);
            if(strcmp(ipbuf,clientip)==0){
                if(timestamp + blacklistcooldown >= (unsigned long)time(NULL)){
                    attempt = 0;
                    sendsocket("ERR YOU ARE TEMPORALLY BLOCKED!\n");
                    closefile(f);
                    return 1;
                }
            }
            printf("IP: %s TIMESTAMP: %lu\n",ipbuf,timestamp);
        }

        closefile(f);
        printf("LOGIN ATTEMPT NR %d STARTED!\n",attempt);
        sendsocket("OK\n");
        return 0;
    }
    else{
        sendsocket("ERR\n");
        return 1;
    }
}
int enterusr(char* username){
    if(strlen(username) > 9){
        sendsocket("ERR\n");
        return 1;
    }
    else{
        strcpy(usr,username);
        //usr[strlen(usr)-1] = 0;
        sendsocket("OK\n");
        return 0;
    }

}
int enterpwd(char* password){
    if(strlen(password) > 32){
        sendsocket("ERR\n");
        return 1;
    }
    else{
      strcpy(pwd,password);
      //pwd[strlen(pwd)-1] = 0;
      sendsocket("OK\n");
      //for the LDAP check the \n char at the end has to be removed!
      char usrc[100];
      strcpy(usrc,"uid=");
      strcat(usrc, usr);
      usrc[strcspn(usrc,"\n")] = 0;
      strcat(usrc, ",ou=People,dc=technikum-wien,dc=at");
      printf("%s\n",usrc);

      char pwdc[32];
      strcpy(pwdc,pwd);
      pwdc[strcspn(pwdc, "\n")] = 0;

      char fltr[100];
      strcpy(fltr,"(uid=");
      strcat(fltr,usr);
      fltr[strcspn(fltr,"\n")] = 0;
      strcat(fltr, ")");
      printf("%s\n",fltr);

      //printf("USERNAME: %s PASSWORD: %s\n",usr,pwd);
      
      int returnval = ldapconnect(usrc,pwdc,fltr);
      if(returnval == 0)
        sendsocket("\nOK\n");
      else
        sendsocket("\nERR\n");
      return returnval;
    }
}
