/* myserver.c */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <myserver.h>
#include "statemachine.h"
#include "filehandler.h"
#define BUF 1024
//#define PORT 6543

char* msgbuf;

int create_socket, new_socket;
socklen_t addrlen;
char buffer[BUF];
int size;
struct sockaddr_in address, cliaddress;

int main (int argc, char **argv) {

  if( argc < 3 ){
     printf("Usage: %s ServerPort DbPfad\n", argv[0]);
     exit(EXIT_FAILURE);
  }
  strcpy(basepath,argv[2]);
  strcat(basepath,"/");
  createpath(basepath);

  create_socket = socket (AF_INET, SOCK_STREAM, 0);

  memset(&address,0,sizeof(address));
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons (atoi(argv[1]));

  if (bind ( create_socket, (struct sockaddr *) &address, sizeof (address)) != 0) {
     perror("bind error");
     return EXIT_FAILURE;
  }
  listen (create_socket, 5);

  addrlen = sizeof (struct sockaddr_in);

  while (1) {
     printf("Waiting for connections on port %s...\n",argv[1]);
     new_socket = accept ( create_socket, (struct sockaddr *) &cliaddress, &addrlen );

     pid_t pid;
           pid = fork();

           if(pid == -1){
             return EXIT_FAILURE;
           }
           else if(pid == 0){// child process
             printf("Hi I am a new Process!\n" );
             if (new_socket > 0)
             {
                clientip = inet_ntoa (cliaddress.sin_addr);
                printf ("Client connected from %s:%d...\n", inet_ntoa (cliaddress.sin_addr),ntohs(cliaddress.sin_port));
                strcpy(buffer," Welcome to myserver!\n");
                //send(new_socket, buffer, strlen(buffer),0);
                sendsocket(buffer);
                flush();
             }
             do {
                size = recv (new_socket, buffer, BUF-1, 0);
                if( size > 0)
                {
                   buffer[size] = '\0';
                   //printf ("Message received: %s\n", buffer);
                   parsemsg(buffer);
                }
                else if (size == 0)
                {
                   printf("Client closed remote socket\n");
                   resetstates();
                   return EXIT_SUCCESS;
                   break;
                }
                else
                {
                   perror("recv error");
                   return EXIT_FAILURE;
                }
             } while (strncmp (buffer, "quit", 4)  != 0);
             close (new_socket);
           }

  }

  close (create_socket);
  return EXIT_SUCCESS;
}

void sendsocket(char * msg){

  msgbuf = msg;
}

void flush(){
  char sendstr[BUF];
  sprintf(sendstr,"%d,%d#",state,cmd);
  strcat(sendstr,msgbuf);
  send(new_socket, sendstr, strlen(sendstr),0);
}
