#include "send.h"
#include "filehandler.h"
#include <string.h>
#include <myserver.h>
#include <unistd.h>

FILE * f;

int lastid = 0;

char mysender[16];
char* path;

int setsender(char * sender){
  printf("SENDER: %s",sender);
  if(strlen(sender) > 9){
    sendsocket("ERR\n");
    return 1;
  }
  else{
    strcpy(mysender,sender);
    sendsocket("OK\n");
    return 0;
  }
}

int setreceiver(char * receiver){
  printf("RECEIVER: %s", receiver);
  if(strlen(receiver) > 9){
    sendsocket("ERR\n");
    return 1;
  }
  else{
    path = getuserpath(receiver);
    lastid = getlastid(path,1);
    if(lastid == -1){
      sendsocket("ERROR CREATING MESSAGE!\n");
      return 1;
    }

    f = openfile(path, lastid +1,"a");
    if(strstr(receiver,"..") != NULL){
      sendsocket("NICE TRY!\n");
      return 1;
    }
    else{
      writemsg(f,mysender);
      sendsocket("OK\n");
      return 0;
    }
  }
}

int setsubject(char * subject){
  printf("SUBJECT: %s", subject);
  if(strlen(subject) > 81){
    sendsocket("ERR\n");
    return 1;
  }
  else{
    writemsg(f,subject);
    sendsocket("OK\n");
    return 0;
  }
}

int setmessage(char * message){
  printf("MESSAGE: %s", message);
  sendsocket("OK\n");
  if(strcmp(message,".\n") == 0){
    closefile(f);
    return 1;
  }
  else{
    writemsg(f,message);
    return 0;
  }

}
