/* myclient.c */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#define BUF 1024
// #define PORT 6543

int state = 0;
int cmd = -1;

struct termios term, term_orig;

int main (int argc, char **argv) {
  int create_socket;
  char buffer[BUF];
  char recbuf[BUF];
  struct sockaddr_in address;
  int size;
  int firstlogin = 1;

  if( argc < 3 ){
     printf("Usage: %s ServerAdresse ServerPort\n", argv[0]);
     exit(EXIT_FAILURE);
  }

  if ((create_socket = socket (AF_INET, SOCK_STREAM, 0)) == -1)
  {
     perror("Socket error");
     return EXIT_FAILURE;
  }

  memset(&address,0,sizeof(address));
  address.sin_family = AF_INET;
  address.sin_port = htons (atoi(argv[2]));
  inet_aton (argv[1], &address.sin_addr);

  if (connect ( create_socket, (struct sockaddr *) &address, sizeof (address)) == 0)
  {
     printf ("Connection with server (%s) established\n", inet_ntoa (address.sin_addr));
     size=recv(create_socket,buffer,BUF-1, 0);
     if (size>0)
     {
        buffer[size]= '\0';
        //printf("%s",buffer);
     }
  }
  else
  {
     perror("Connect error - no server available");
     return EXIT_FAILURE;
  }

  tcgetattr(STDIN_FILENO, &term);
  term_orig = term;
  term.c_lflag &= ~ECHO;

  do {
     switch(cmd){
       case -1:
        switch(state){
        case 0:
            printf("Enter LOGIN to use the service! ");
            tcsetattr(STDIN_FILENO, TCSANOW, &term_orig);
          break;
          case 1:
            printf("Enter your username: ");
          break;
          case 2:
            printf("Enter your password: ");
            tcsetattr(STDIN_FILENO, TCSANOW, &term);
          break;
        }
       break;
       case 0:
        printf("Enter command (SEND,LIST,READ,DEL): ");
        tcsetattr(STDIN_FILENO, TCSANOW, &term_orig);
       break;
       case 1:
        switch(state){
          case 0:
            printf("Enter recepient: ");
          break;
          case 1:
            printf("Enter subject: ");
          break;
          case 2:
            printf("Enter message: ");
          break;
        }
       break;
       //cmd 2 is list but does not need a handler anymore
       case 3:
        printf("Enter message number: ");
       break;
       case 4:
        printf("Enter message number: ");
       break;
     }
     if(cmd == -1 && state == 0 && firstlogin == 1){
       send(create_socket, "LOGIN\n", strlen ("LOGIN\n"), 0);
       printf("LOGIN\n");
       firstlogin = 0;
     }
     else{
      fgets (buffer, BUF, stdin);
      send(create_socket, buffer, strlen (buffer), 0);
     }

     size=recv(create_socket,recbuf,BUF-1, 0);
     if (size>0)
     {
        recbuf[size]= '\0';
        substring(recbuf);
        printf("%s",recbuf);
     }
  }
  while (strcmp (buffer, "QUIT\n") != 0);
  close (create_socket);
  return EXIT_SUCCESS;
}

void substring(char* s) {
   int bracket = 0;
   int pound = 0;
   char value[8];

   for(int i = 0; i < strlen(s);i++){
     if(s[i] == ','){
       value[i] = '\0';
       bracket = i;
       break;
     }
     else
      value[i]=s[i];
   }

   state = atoi(value);

   for(int i = 0; i < strlen(s);i++){
     if(s[i + bracket + 1] == '#'){
       value[i] = '\0';
       pound = i + bracket + 1;
       break;
     }
     else
      value[i]=s[i + bracket + 1];
   }

   cmd = atoi(value);
   memmove(s, s+pound+1, strlen(s));
}
